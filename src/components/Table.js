import React from "react";


function Table(props) {
  // Use this for showing thead
  const fields = [
    "#",
    "short_name",
    "age",
    "nationality",
    "club",
    "overall",
    "value",
    "preferred_foot",
    "team_position"
  ]; 
  return (
    <table id="players-table" className="table table-bordered">
      <thead>
        <tr>
        {
           // Show table's header code
          fields.map((field,index) => <th scope="col" key={index}>{field}</th>)
        }
        </tr>
      </thead>
      <tbody>
        {
          // Show item's row code
          props.tableFilter.map((player,index) => 
          <tr key={index}>
            <th scope="row">{index+1}</th>
            <td>{player.short_name}</td>
            <td>{player.age}</td>
            <td>{player.nationality}</td>
            <td>{player.club}</td>
            <td>{player.overall}</td>
            <td>{player.value}</td>
            <td>{player.preferred_foot}</td>
            <td>{player.team_position}</td>
          </tr>)
        }
      </tbody>
    </table>
  );
}

export default Table;
