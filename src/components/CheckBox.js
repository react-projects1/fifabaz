import React ,{useState} from "react";

function Checkbox(props) {
  
  // Sorting buttons 
  const [checked , setChecked] = useState(false);
  
  const changeFilter = (e) => {
    setChecked(!checked);
    props.changeFilters(checked,e.target.value , props.type);
  };


  return (
    <div className="form-check form-check-inline" >
      <input className="form-check-input" type="checkbox"  value={props.item} checked={checked} onChange={changeFilter}/>
      <label className="form-check-label" htmlFor="inlineCheckbox2">{props.item}</label>
    </div>
  );
}

export default Checkbox;
