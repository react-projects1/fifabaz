import React from "react";
import nationality from './../data/Nationality';
import clubs from './../data/Clubs';
import team_positions from './../data/TeamPositions';
import CheckBox from './CheckBox';




function FilterBox(props) {

  // const filters = ["nationality", "club", "team_position"];
  return (
    <div className="filter-box">
      <div>
        <h5 className="card-title">Filter Box</h5>
        {
          // It's Bootstrap accordion example
          // You have to change this part base on description
        }
        <div className="accordion" id="accordionExample">
          <div className="card">
            <div className="card-header" id="headingOne">
              <h2 className="mb-0">
                <button
                  id="btn-FILTER_NAME-collapse"
                  className="btn"
                  type="button"
                  data-toggle="collapse"
                  data-target="#collapseOne"
                  aria-expanded="true"
                  aria-controls="collapseOne"
                >
                 nationality
                </button>
              </h2>
            </div>
            <div
              id="collapseOne"
              className="collapse show"
              aria-labelledby="headingOne"
              data-parent="#accordionExample"
            >
              <div className="card-body">
                {nationality.map((item,index ) => <CheckBox key={index} item= {item} type="nationality" changeFilters={props.changeFilters}/>)}
              </div>
            </div>
          </div>
          <div className="card">
            <div className="card-header" id="headingTwo">
              <h2 className="mb-0">
                <button
                  className="btn"
                  type="button"
                  data-toggle="collapse"
                  data-target="#collapseTwo"
                  aria-expanded="false"
                  aria-controls="collapseTwo"
                >
                  club
                </button>
              </h2>
            </div>
            <div
              id="collapseTwo"
              className="collapse"
              aria-labelledby="headingTwo"
              data-parent="#accordionExample"
            >
              <div className="card-body">
                {clubs.map((item,index) => <CheckBox key={index} item= {item} type="clubs" changeFilters = {props.changeFilters}/>)}
              </div>
            </div>
          </div>
          <div className="card">
            <div className="card-header" id="headingThree">
              <h2 className="mb-0">
                <button
                  className="btn"
                  type="button"
                  data-toggle="collapse"
                  data-target="#collapseThree"
                  aria-expanded="false"
                  aria-controls="collapseThree"
                >
                  team-position
                </button>
              </h2>
            </div>
            <div
              id="collapseThree"
              className="collapse"
              aria-labelledby="headingThree"
              data-parent="#accordionExample"
            >
              <div className="card-body">
                {team_positions.map((item,index) =><CheckBox key={index} type="team_positions" item= {item} changeFilters = {props.changeFilters}/>)}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FilterBox;
