import React , {useState} from "react";

function SortBox(props) {
  
  // Sorting buttons 
  const fields = ["age", "short_name", "value"];
  const [sortType , setSortType] = useState(false)

  const changeSelectedSort = (e) => {
    setSortType(!sortType)
    props.changeSort(e.target.id,sortType);
  };

  // const setClassName = (/*YOUR_PARAMETERS*/) => {
  //  // Default sorted => "btn-outline-primary" Non selected button (Third Click)
  //  // Descending sorted => "btn-success" First Click
  //  // Ascending sorted => "btn-info" Second Click
  //  // Your Code ...
  // };
  
  return (
    <div id="sort-box-container" className="d-flex py-2">
      {fields.map((f,index) => <button
      id={f}
      type="button"
      className="btn mx-2 sort-btn"
      key = {index}
      onClick = {changeSelectedSort}
      >
      {f}
      </button>)}
    </div>
  );
}

export default SortBox;
