import React , {useState} from "react";
import Table from "../components/Table";
import FilterBox from "../components/FilterBox";
import SortBox from "../components/SortBox";
import Players from './../data/Players';



function App() {
   //define your state
   const [players , setPlayaers] = useState(Players);
  
   const [filterNationality , setFilternationality] = useState([]);
   const [filterClub , setFilterclub] = useState([]);
   const [filterPosition , setFilterposition] = useState([]);
   
  
  const changeFilters = (newState,value ,type) => {
    if(!newState){
      switch (type) {
        case 'nationality':
          setFilternationality([...filterNationality , value]);
          break;
        case 'clubs':
          setFilterclub([...filterClub , value] );
          break;
        case 'team_positions':
          setFilterposition([...filterPosition , value]);
          break;
      
        default:
          break;
      }
    }
    // setPlayaers([...playerFilter]);
  }
  
  const changeSort = (id,sort) => {
    let compare
    if(id === 'age'){
      compare = (a,b) => (a.age < b.age) ? -1 : ((a.age > b.age) ? 1 :0)
    }
    if(id === 'value'){
      compare = (a,b) => (a.value < b.value) ? -1 : ((a.value > b.value) ? 1 :0)
    }
    if(id === 'short_name'){
      compare = (a,b) => (a.short_name < b.short_name) ? -1 : ((a.short_name > b.short_name) ? 1 :0)
    }
     
    if(!sort){
      //صعودی
      let ASC = Players.sort(compare);
      setPlayaers([...ASC]);
    
    }else{
      // نزولی
      let DES = Players.reverse();
      setPlayaers([...DES]);
    }
  }


  let playerFilter = Players;
  
 
  if(filterNationality.length){
    playerFilter = playerFilter.filter(player => {
      if(filterNationality.indexOf(player.nationality) !== -1) return true;
      return false;
    });
  }

  if(filterClub.length){
    playerFilter = playerFilter.filter(player => {
      if(filterClub.indexOf(player.club) !== -1) return true;
      return false;
    });
  }

  if(filterPosition.length){
    playerFilter = playerFilter.filter(player => {
      if(filterPosition.indexOf(player.team_position) !== -1) return true;
      return false;
    });
  }
 
  
  return (
    <div className="container-fluid my-3">
      <div className="row">
        <div className="col-lg-2 col-12">
          <FilterBox changeFilters = {changeFilters} />
        </div>
        <div className="col-lg-10 col-12">
          <SortBox changeSort={changeSort} />
          {/* {renderTable()} */}
          <Table tableFilter = {playerFilter}/>
        </div>
      </div>
    </div>
  );
}

export default App;
